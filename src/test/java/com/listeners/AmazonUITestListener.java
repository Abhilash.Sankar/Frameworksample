package com.listeners;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentTest;
import com.ui.utils.BrowserUtility;

public class AmazonUITestListener implements ITestListener {

	private ExtentTest extentTest;

	@Override
	public void onTestStart(ITestResult result) {
		System.out.println("************** Starting the Test " + result.getMethod().getMethodName());

		extentTest = ExtentManager.getInstance().createTest(result.getMethod().getDescription());

	}

	@Override
	public void onTestSuccess(ITestResult result) {
		System.out.println("************** Test Passed : " + result.getMethod().getMethodName());

		extentTest.addScreenCaptureFromPath(BrowserUtility.takeScreenshot(result.getMethod().getDescription()),
				result.getMethod().getDescription());

		extentTest.pass("Test Executed Succesfully!!!");

	}

	@Override
	public void onTestFailure(ITestResult result) {
		System.out.println("************** Test Failed : " + result.getMethod().getMethodName());
		extentTest.fail("Test is failed!!!");
		extentTest.addScreenCaptureFromPath(BrowserUtility.takeScreenshot(result.getMethod().getDescription()),
				result.getMethod().getDescription());
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		System.out.println("************** Test Skipped : " + result.getMethod().getMethodName());
		extentTest.skip("Test is skipped!!!");
		extentTest.addScreenCaptureFromPath(BrowserUtility.takeScreenshot(result.getMethod().getDescription()),
				result.getMethod().getDescription());

	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStart(ITestContext context) {
		System.out.println("************** Starting TestNG Suite **************");

	}

	@Override
	public void onFinish(ITestContext context) {
		System.out.println("************** Completed TestNG Suite **************");
		ExtentManager.getInstance().flush();

	}

}
