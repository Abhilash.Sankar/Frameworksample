package com.listeners;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;


import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

public class ExtentManager {
	private static ExtentReports extentReports;
	
	public static ExtentReports getInstance() {
		File reportFolder = new File(System.getProperty("user.dir") + "//reports");
		
		if (!reportFolder.isDirectory()) {
		    reportFolder.mkdirs();
		}
		
	    if (extentReports == null) {
	        Date date = new Date();
	        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
	        String dateString = dateFormat.format(date);
	        extentReports = createReportInstance("Report_" + dateString + ".html");
	    }
	    return extentReports;
	}

	private static ExtentReports createReportInstance(String file) {
		File reportFile = new File(System.getProperty("user.dir") + "//reports//"+file);
		ExtentSparkReporter extentSparkReporter = new ExtentSparkReporter(reportFile);
		extentReports = new ExtentReports();
		extentReports.attachReporter(extentSparkReporter);
		return extentReports;
	}

}
