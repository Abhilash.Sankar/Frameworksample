package com.listeners;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class AmazonRetryAnalyzer implements IRetryAnalyzer {
	int count = 1;
	int maxCount =2;

	@Override
	public boolean retry(ITestResult result) {
		if(count<=maxCount) {
			return true;
		}
		return false;
	}

}
