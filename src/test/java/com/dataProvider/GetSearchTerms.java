package com.dataProvider;
import java.util.Iterator;
import java.util.List;

import org.testng.annotations.DataProvider;

import com.ui.utils.BrowserUtility;

public class GetSearchTerms {
    @DataProvider(name = "searchitems from csv")
    public static Iterator<Object[]> searchTermsList() {
        List<String[]> records = BrowserUtility.readCSV(System.getProperty("user.dir") + "//testdata//searchitems.csv");
        records.remove(0); // Remove the heading row
        return records.stream().map(record -> new Object[]{record[0]}).iterator();
    }

}