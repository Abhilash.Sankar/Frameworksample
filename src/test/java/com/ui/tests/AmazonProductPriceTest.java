package com.ui.tests;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.testng.Assert;
import org.testng.annotations.*;

import com.ui.pages.CartDetails;
import com.ui.pages.HomePageSearch;
import com.ui.pages.ProceedToBuy;
import com.ui.pages.ProductMainPage;
import com.ui.pages.ResultsPage;
import com.ui.utils.Browser;

@Listeners(com.listeners.AmazonUITestListener.class)

public class AmazonProductPriceTest {

	private HomePageSearch onHomePageSearch;
	private ResultsPage resultsPage;
	private ProductMainPage productMainPage;
	private ProceedToBuy proceedToBuy;
	private CartDetails cartDetails;

	@BeforeTest(description = "Open the browser and load website", alwaysRun = true)
	public void setup() {
		onHomePageSearch = new HomePageSearch(Browser.CHROME);

	}

	@Test(description = "Test to verify the search functionality in the home page", groups = { "smoke",
			"Regression" }, priority = 1, dataProvider = "searchitems from csv", dataProviderClass = com.dataProvider.GetSearchTerms.class)
	public void verifyHomePageSearch(String itemToSearch) {
		
		resultsPage = onHomePageSearch.searchWith(itemToSearch);
		Assert.assertTrue(resultsPage.getCurrentUrl().contains(itemToSearch.split(" ")[0]));

	}

	@Test(description = "To verify if search results are displayed as expected", groups = { "smoke",
			"Regression" }, priority = 2)
	public void verifyResultsPage() {
		System.out.println("verifyResultPage");

		productMainPage = resultsPage.sortItem();

		MatcherAssert.assertThat(resultsPage.getSortValue(), CoreMatchers.containsString("High to Low"));

		productMainPage = resultsPage.selectItem();

	}

	@Test(description = "To verify if buttons are displayed as expected in the Product Page", groups = { "smoke",
			"Regression" }, priority = 3, retryAnalyzer = com.listeners.AmazonRetryAnalyzer.class)
	public void verifyProductMainPage() {
		productMainPage.enableProductWarranty();
		productMainPage.findAddToCartButton();

		Assert.assertTrue(productMainPage.findAddToCartButton());
		proceedToBuy = productMainPage.addItemToCart();
	}

	@Test(description = "To verify if the product is added to the cart", groups = { "smoke",
			"Regression" }, priority = 4)
	public void verifyTheCart() {
		cartDetails = proceedToBuy.goToCart();
		int numberOfProducts = proceedToBuy.productsInCart();
		Assert.assertTrue(numberOfProducts >= 1, "Product is added to the cart");
		cartDetails = proceedToBuy.goToCart();
	}

	@Test(description = "To verify if the total price of the products in the cart is displayed", groups = { "smoke",
			"Regression" }, priority = 5)
	public void verifyPriceOfProductsInTheCart() {
		String finalCartPrice = cartDetails.displayCartValue();

		Assert.assertTrue(finalCartPrice.matches("\\s*\\d{1,3}(,\\d{2,3})*(\\.\\d+)?\\s*"),
				"The value is not in Indian currency format: " + cartDetails.displayCartValue());

	}

	@AfterTest(description = "TearDown", alwaysRun = true)
	public void closeTheBrowser() {
		resultsPage.terminateSession();
	}

}
