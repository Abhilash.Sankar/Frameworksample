package com.ui.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public abstract class BrowserUtility {
	protected WebDriver wd;
	public static WebDriver wdScreenshot;
	private WebDriverWait wait;

	public BrowserUtility(WebDriver wd) {
		this.wd = wd;
		wdScreenshot = wd;
		wait = new WebDriverWait(this.wd, Duration.ofSeconds(15L));

	}

	public BrowserUtility(Browser browser) {
		if (browser == Browser.CHROME) {
			wd = new ChromeDriver();
			wait = new WebDriverWait(this.wd, Duration.ofSeconds(15L));
		} else if (browser == Browser.FIREFOX) {
			wd = new FirefoxDriver();
			wait = new WebDriverWait(this.wd, Duration.ofSeconds(15L));
		} else if (browser == Browser.EDGE) {
			wd = new EdgeDriver();
			wait = new WebDriverWait(this.wd, Duration.ofSeconds(15L));
		} else if (browser == Browser.SAFARI) {
			wd = new SafariDriver();
			wait = new WebDriverWait(this.wd, Duration.ofSeconds(15L));
		}
		wdScreenshot = wd;
	}

	public WebDriver getWd() {
		return wd;
	}

	public void goToWebsite(String url) {

		wd.get(url);
	}

	public void maximiseWindow() {
		wd.manage().window().maximize();
	}

	public void terminateSession() {
		wd.quit();
	}

	public void websiteCompatibilityTest(By locator) {
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
			System.out.println("Desktop version of the website is NOT Displayed!!! Please Restart Testing");
			terminateSession();
		} catch (TimeoutException e) {
			// Skip the step if Desktop version of the Website is displayed
		}

	}

	public void clickButton(By locator) {
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		element.click();
	}

	public void enterTextInto(By locator, String textToEnter) {
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		element.clear();
		element.sendKeys(textToEnter);
	}

	public String getTheValueOf(By locator) {
		WebElement textData = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		String displayOutput = textData.getText();
		return displayOutput;
	}

	public void switchToNewWindow() {
		Set<String> s = wd.getWindowHandles();
		ArrayList<String> ar = new ArrayList<String>(s);
		if (ar.size() > 1)
			wd.switchTo().window((String) ar.get(1));
		else
			wd.switchTo().window((String) ar.get(0));
	}

	public List<WebElement> fetchAllTheElements(By locator) {
		List<WebElement> elementsList = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));

		return elementsList;
	}

	public void enableWarranty() {
		try {
			WebElement enableWarranty = wait
					.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='mbb-offeringID-1']")));
			enableWarranty.click();

		} catch (TimeoutException e) {
			// Skip the step if warranty option is not displayed
		}
	}

	public void scrollToFindElement(By locator) {
		WebElement element = wd.findElement(locator);
		JavascriptExecutor executor = (JavascriptExecutor) wd;
		executor.executeScript("arguments[0].scrollIntoView(true);", element);
	}

	public boolean isButtonEnabled(By locator) {
		WebElement buttonElement = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		return buttonElement.isEnabled();
	}

	public void closeSideSheets() {
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("attach-desktop-sideSheet")));
			WebElement closeSideSheet = wait
					.until(ExpectedConditions.visibilityOfElementLocated(By.id("attach-close_sideSheet-link")));
			closeSideSheet.click();
		} catch (TimeoutException e) {

			// Skip the step if the element is not displayed
		}
	}

	public String getCurrentUrl() {
		return wd.getCurrentUrl();
	}

	public static String readFromPropertiesFile(String key, String testRegion) {
		File propFile = new File(System.getProperty("user.dir") + "//config//" + testRegion + ".properties");
		String data = null;

		try {
			Reader propReader = new FileReader(propFile);
			Properties properties = new Properties();
			properties.load(propReader);
			data = properties.getProperty(key);
		} catch (FileNotFoundException e1) {

			e1.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return data;

	}

	public static String takeScreenshot(String fileName) {
		TakesScreenshot screenshot = (TakesScreenshot) wdScreenshot;
		File src = screenshot.getScreenshotAs(OutputType.FILE);
		String path = System.getProperty("user.dir") + "\\screenshots\\" + fileName + ".PNG";
		File dest = new File(System.getProperty("user.dir") + "\\screenshots\\" + fileName + ".PNG");

		try {
			FileUtils.copyFile(src, dest);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return path;
	}

	public static List<String[]> readCSV(String filename) {
		List<String[]> data = new ArrayList<String[]>();
		String testRow;
		try {

			BufferedReader br = new BufferedReader(new FileReader(filename));
			while ((testRow = br.readLine()) != null) {
				String[] line = testRow.split(",");
				data.add(line);
			}
		} catch (FileNotFoundException e) {
			System.out.println("ERROR: File not found " + filename);
		} catch (IOException e) {
			System.out.println("ERROR: Could not read " + filename);
		}
		return data;
	}

}
