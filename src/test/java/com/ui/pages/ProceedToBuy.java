package com.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.ui.utils.BrowserUtility;

public class ProceedToBuy extends BrowserUtility {

	private static final By NUMBEROFPRODUCTSINCART = By.xpath("//span[@id='sc-subtotal-label-activecart']");
	private static final By GOTOTHECART = By.xpath("//div[@id='nav-cart-count-container']/..");

	public ProceedToBuy(WebDriver wd) {
		super(wd);
		// TODO Auto-generated constructor stub
	}

	public int productsInCart() {
		scrollToFindElement(NUMBEROFPRODUCTSINCART);
		String numberOfProducts = getTheValueOf(NUMBEROFPRODUCTSINCART);
		String numberOfItemsString = numberOfProducts.replaceAll("\\D+", "");
		int numberOfItems = Integer.parseInt(numberOfItemsString);
		return numberOfItems;
	}
public CartDetails goToCart() {
	scrollToFindElement(GOTOTHECART);
	clickButton(GOTOTHECART);
	return new CartDetails(getWd());
	
}
}
