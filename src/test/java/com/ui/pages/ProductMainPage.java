package com.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.ui.utils.BrowserUtility;

public class ProductMainPage extends BrowserUtility {
	private static final By ADDITEMTOCART = By.xpath("//input[@id='add-to-cart-button']");

	public ProductMainPage(WebDriver wd) {
		super(wd);
		// TODO Auto-generated constructor stub
	}

	public void enableProductWarranty() {
		enableWarranty();
	}

	public boolean findAddToCartButton() {
		scrollToFindElement(ADDITEMTOCART);
		return isButtonEnabled(ADDITEMTOCART);
	}

	public ProceedToBuy addItemToCart() {

		clickButton(ADDITEMTOCART);
		closeSideSheets();
		return new ProceedToBuy(getWd());
	}

	
}
