package com.ui.pages;

import org.openqa.selenium.By;

import com.ui.utils.Browser;
import com.ui.utils.BrowserUtility;

public class HomePageSearch extends BrowserUtility{
	private static final By WEBSITELOGOLITEVERSION = By.xpath("//a[@id='nav-bb-logo']");
	private static final By SEARCHBOXLOCATOR =By.id("twotabsearchtextbox");
	private static final By SEARCHBUTTONLOCATOR = By.id("nav-search-submit-button");
	
	public HomePageSearch(Browser browser) {
		super(browser);
		goToWebsite(readFromPropertiesFile("URL", "qa"));
		maximiseWindow();
		
	}
	public void testCompatibilityTest() {
		websiteCompatibilityTest(WEBSITELOGOLITEVERSION);
		
	}
	
	public ResultsPage searchWith(String itemToSearch) {
		clickButton(SEARCHBOXLOCATOR);
		enterTextInto(SEARCHBOXLOCATOR, itemToSearch);
		clickButton(SEARCHBUTTONLOCATOR);
		return new ResultsPage(getWd());
	}
}
