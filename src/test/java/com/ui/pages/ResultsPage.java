package com.ui.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.ui.utils.BrowserUtility;

public class ResultsPage extends BrowserUtility {

	private static final By SEARCHRESULTSORTER = By.xpath("//span[@id='a-autoid-0-announce']");
	private static final By SORTTYPECHOOSER = By.xpath("//a[contains(text(),'High to Low')]");
	private static final By SEARCHRESULTLOCATORINGRID = By.xpath(
			"//div[contains(@cel_widget_id, 'MAIN-SEARCH_RESULTS-3')]//preceding-sibling::*[contains(@class, 'a-size-medium')]");
	private static final By SEARCHRESULTLOCATORINLIST = By.xpath(
			"//div[contains(@cel_widget_id, 'MAIN-SEARCH_RESULTS')]");
	private String sortvalue;

	public ResultsPage(WebDriver wd) {
		super(wd);

	}

	public ProductMainPage sortItem() {

		clickButton(SEARCHRESULTSORTER);
		clickButton(SORTTYPECHOOSER);
		return new ProductMainPage(getWd());
	}

	public String getSortValue() {
		sortvalue = getTheValueOf(SEARCHRESULTSORTER);
		return sortvalue;
	}

	public ProductMainPage selectItem() {
		try {
			clickButton(SEARCHRESULTLOCATORINGRID);
			switchToNewWindow();
		} catch (Exception e) {
			// If the first element is not found or not clickable, locate all elements and
			// click on the first one
			
			List<WebElement> elements = fetchAllTheElements(SEARCHRESULTLOCATORINLIST);
			elements.get(0);

			clickButton(SEARCHRESULTLOCATORINLIST);
		switchToNewWindow();

	}
		return new ProductMainPage(getWd());
}}