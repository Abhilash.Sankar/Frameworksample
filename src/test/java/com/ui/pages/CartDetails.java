package com.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.ui.utils.BrowserUtility;

public class CartDetails extends BrowserUtility {
	private static final By TOTALCOST = By.xpath("//div[contains(@class, 'a-section sc-buy-box-inner-box')]//span[@class='currencyINR']/..");

	public CartDetails(WebDriver wd) {
		super(wd);
		// TODO Auto-generated constructor stub
	}
public String displayCartValue() {
	String finalPrice = getTheValueOf(TOTALCOST);
	return finalPrice;
}
}
