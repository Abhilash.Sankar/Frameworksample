# **Amazon UI Testing Framework**

## Prerequisites

- Java 1.8
- Maven
- TestNG
- Selenium
- OpenCSV

## Features

- Performs UI automation of Amazon website. It searches for the highest cost product based on the provided search term, adds it to the cart, retrieves the price, and verifies if it is displayed in the correct currency format.


## Tech

- Selenium WebDriver
- TestNG
- Apache Commons IO
- Hamcrest
- ExtentReports

## Adding New Dependencies

New Dependencies can be added in pom.xml under dependencies tag.

Please note that this automation script is for educational purpose only and should be used responsibly and in accordance with Amazon's terms of service.
